﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebsiteProDev.Repository.CreateImage
{
    public class ConvertToByte
    {
        public static byte[] ConvertingImageToByte(string imgagePath)
        {
            return System.IO.File.ReadAllBytes(imgagePath);
        }
    }
}
