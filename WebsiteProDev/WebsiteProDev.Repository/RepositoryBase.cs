﻿using WebsiteProDev.DataAccess.Context;

namespace WebsiteProDev.Repository
{
    public abstract class RepositoryBase<T>
    {
        public readonly ProDevDbContext _context;

        public RepositoryBase(ProDevDbContext context)
        {
            _context = context;
        }
    }
}