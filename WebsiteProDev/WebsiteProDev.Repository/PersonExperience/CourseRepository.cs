﻿//using Microsoft.AspNetCore.Http;
//using WebsiteProDev.DataAccess.Context;
//using WebsiteProDev.DataLayer.Respone;
//using WebsiteProDev.Model.Course;

//namespace WebsiteProDev.Repository.Course
//{
//    public class CourseRepository : RepositoryBase<CourseRepository>, IExperienceRepository
//    {
//        public CourseRepository(ProDevDbContext context) : base(context)
//        {
//        }

//        public Response<string> CreateCourse(CourseModel model)
//        {
//            Response<string> response = new Response<string>();
//            try
//            {
//                WebsiteProDev.DataLayer.Entity.Course course = new WebsiteProDev.DataLayer.Entity.Course();
//                course.CourseName = model.CourseName;
//                course.CourseDescription = model.CourseDescription;
//                course.Status = model.Status;
//                course.Price = model.Price;
//                course.ApprovedAt = model.ApprovedAt;
//                course.ApprovedBy = model.ApprovedBy;
//                course.LanguageId = model.LanguageId;
//                _context.Course.Add(course);
//                _context.SaveChanges();
//                response.Message = "Thêm mới thành công";
//                response.Code = StatusCodes.Status200OK;
//            }
//            catch (Exception ex)
//            {
//                response.Message = ex.Message;
//                response.Code = StatusCodes.Status400BadRequest;
//            }
//            return response;
//        }

//        public Response<string> DeleteCourse(CourseModel course)
//        {
//            throw new NotImplementedException();
//        }

//        public Response<CourseModel> GetAllCourse()
//        {
//            throw new NotImplementedException();
//        }

//        public Response<CourseModel> GetCourseById(Guid courseId)
//        {
//            throw new NotImplementedException();
//        }

//        public Response<string> UpdateCourse(CourseModel course)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}