﻿using WebsiteProDev.DataLayer.Entity;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Course;

namespace WebsiteProDev.Repository.Course
{
    public interface ICourseRepository
    {
        Response<string> CreateCourse(CourseModel course);

        Response<string> UpdateCourse(CourseModel course);


        Response<CourseViewModel> GetAllCourse();

        Response<CourseViewModel> GetCourseById(Guid Id);
        Response<string> DeleteCourse(Guid id);
    }
}