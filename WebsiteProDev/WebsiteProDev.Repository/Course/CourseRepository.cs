﻿using Microsoft.AspNetCore.Http;
using WebsiteProDev.Common;
using WebsiteProDev.DataAccess.Context;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Course;

namespace WebsiteProDev.Repository.Course
{
    public class CourseRepository : RepositoryBase<CourseRepository>, ICourseRepository
    {
        private IHttpContextAccessor _httpContextAccessor;

        public CourseRepository(ProDevDbContext context) : base(context)
        {
            
        }

        public Response<string> CreateCourse( CourseModel model)
        {
            Response<string> response = new Response<string>();
            try
            {
                var path = Path.Combine("Image",model.ImageData.FileName);
                WebsiteProDev.DataLayer.Entity.Course course = new WebsiteProDev.DataLayer.Entity.Course();
                course.CourseName = model.CourseName;
                course.CourseDescription = model.CourseDescription;
                course.Status = model.Status;
                course.Price = model.Price;
                // course.ApprovedAt = DateTime.UtcNow;
                // course.ApprovedBy = model.ApprovedBy;
               // course.UpdatedBy = Guid.Parse("");

                // Lấy id của user đang cập nhật
                var getIdInstance = new GetId(_httpContextAccessor);
                var createBy = getIdInstance.GetCurrentUserId();
                course.CreatedBy = createBy;

                course.CreatedDate = DateTime.UtcNow;
                //course.UpdatedDate = DateTime.UtcNow;
                course.LanguageId = model.LanguageId;
                course.ImageData = CreateImage.ConvertToByte.ConvertingImageToByte(path);
                _context.Course.Add(course);
                _context.SaveChanges();
                response.Message = "Thêm mới thành công";
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = StatusCodes.Status400BadRequest;
            }
            return response;
        }

        public Response<string> DeleteCourse(Guid id)
        {
            Response<string> response = new Response<string>();
            var courseToDelete = _context.Course.FirstOrDefault(x => x.Id == id);
            try
            {

                if (courseToDelete == null)
                {
                    response.Message = "Không tìm thấy khóa học để xóa";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    _context.Course.Remove(courseToDelete);
                    response.Message = "Xóa khóa học thành công";
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.Message = "Xóa khóa học không thành công";
                response.Message = ex.Message;
            }
            return response;
        }

        public Response<CourseViewModel> GetAllCourse()
        {
            Response<CourseViewModel> response = new Response<CourseViewModel>();
            try
            {
                var coursesFromRepo = _context.Course.ToList();
                var courseViewModels = coursesFromRepo.Select(course => new CourseViewModel
                {
                    // Thực hiện ánh xạ dữ liệu từ đối tượng Course sang CourseViewModel
                    Id = course.Id,
                    CourseName = course.CourseName,
                    CourseDescription = course.CourseDescription,
                    Status = course.Status,
                    Price = course.Price,
                   // CreatedBy = course.CreatedBy,
                   // ApprovedAt = course.ApprovedAt,
                    ApprovedBy = course.ApprovedBy,
                    LanguageId = course.LanguageId,
                    ImageData = course.ImageData
                   
                }).ToList();

                response.DataList = courseViewModels;
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception)
            {
                response.Code=StatusCodes.Status400BadRequest;
            }
            return response;
        }


        public Response<CourseViewModel> GetCourseById(Guid Id)
        {
            Response<CourseViewModel> response = new Response<CourseViewModel>();
            try
            {
                var course = _context.Course.FirstOrDefault(c => c.Id == Id);
                if (course == null)
                {
                    response.Message = "Không tìm thấy khóa học";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    CourseViewModel courseModel = new CourseViewModel
                    {
                        // Gán các thuộc tính của CourseModel từ đối tượng Course
                        Id = course.Id,
                        CourseName = course.CourseName,
                        CourseDescription = course.CourseDescription,
                        Status = course.Status,
                        Price = course.Price,
                        //ApprovedAt = course.ApprovedAt,
                        ApprovedBy = course.ApprovedBy,
                        LanguageId = course.LanguageId,
                        ImageData = course.ImageData
                    };
                   
                    response.Data = courseModel;
                    response.Message = "Lấy thành công";

                }
                return response;
            }
            catch (Exception)
            {
                response.Code = StatusCodes.Status400BadRequest;
                return response;
            }  
        }

        public Response<string> UpdateCourse(CourseModel course)
        {
            Response<string> response = new Response<string>();
            try
            {
                var existingCourse = _context.Course.FirstOrDefault(c => c.Id == course.Id);
                var path = Path.Combine("Image", course.ImageData.FileName);
                bool hasNewImage = false;

                if (existingCourse == null)
                {
                    response.Message = "Không tìm thấy khóa học";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    if (course.ImageData != null && course.ImageData.Length > 0)
                    {
                        // Có ảnh mới
                        existingCourse.ImageData = CreateImage.ConvertToByte.ConvertingImageToByte(path);
                        hasNewImage = true;
                    }

                    // Cập nhật các thuộc tính khác của existingCourse từ course
                    existingCourse.CourseName = course.CourseName;
                    existingCourse.CourseDescription = course.CourseDescription;
                    existingCourse.Status = course.Status;
                    existingCourse.Price = course.Price;
                    existingCourse.LanguageId = course.LanguageId;
                    existingCourse.UpdatedDate = DateTime.UtcNow;

                    // Lấy Id của user đang chỉnh sửa
                    //  var getIdInstance = new GetId(_httpContextAccessor);
                    // var updateBy = getIdInstance.GetCurrentUserId();
                    // existingCourse.UpdatedBy = updateBy;

                    //existingCourse.CreatedDate = course.CreatedDate;
                    // Chỉ lưu lại ảnh nếu có ảnh mới được cung cấp
                    if (hasNewImage)
                    {
                        _context.SaveChanges();
                    }

                    response.Message = "Cập nhật thành công";
                    response.Code = StatusCodes.Status200OK;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = StatusCodes.Status400BadRequest;
            }

            return response;
        }

      

    }
}