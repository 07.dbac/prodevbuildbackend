﻿using Microsoft.AspNetCore.Http;
using WebsiteProDev.DataAccess.Context;
using WebsiteProDev.DataLayer.Entity;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Experience;

namespace WebsiteProDev.Repository.Experience
{
    public class ExperienceRepository : RepositoryBase<ExperienceRepository>, IExperienceRepository
    {
        public ExperienceRepository(ProDevDbContext context) : base(context)
        {
        }

        public Response<string> CreateExperience(ExperienceModel experience)
        {
            Response<string> response = new Response<string>();
            try
            {
                var newExperience = new WebsiteProDev.DataLayer.Entity.Experience
                {
                    Position = experience.Position,
                    Company = experience.Company,
                    CreatedDate = DateTime.UtcNow,
                  //  CreatedBy = currentUser.Id,
                };

                _context.Experience.Add(newExperience);
                _context.SaveChanges();

                response.Message = "Thêm mới thành công";
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = StatusCodes.Status400BadRequest;
            }

            return response;
        }

        public Response<string> UpdateExperience(ExperienceModel experience)
        {
            Response<string> response = new Response<string>();
            try
            {
                var existingExperience = _context.Experience.FirstOrDefault(e => e.Id == experience.Id);

                if (existingExperience == null)
                {
                    response.Message = "Không tìm thấy kinh nghiệm";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    existingExperience.Position = experience.Position;
                    existingExperience.Company = experience.Company;
                    existingExperience.UpdatedDate = DateTime.UtcNow;
                   // existingExperience.UpdatedBy = Get;
                    var createDate = existingExperience.CreatedDate;
                    existingExperience.CreatedDate = createDate;
                    _context.SaveChanges();

                    response.Message = "Cập nhật thành công";
                    response.Code = StatusCodes.Status200OK;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = StatusCodes.Status400BadRequest;
            }

            return response;
        }

        public Response<string> DeleteExperienceById(Guid id)
        {
            Response<string> response = new Response<string>();
            try
            {
                var experienceToDelete = _context.Experience.FirstOrDefault(e => e.Id == id);

                if (experienceToDelete == null)
                {
                    response.Message = "Không tìm thấy kinh nghiệm để xóa";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    _context.Experience.Remove(experienceToDelete);
                    _context.SaveChanges();

                    response.Message = "Xóa kinh nghiệm thành công";
                    response.Code = StatusCodes.Status200OK;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Xóa kinh nghiệm không thành công";
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<ExperienceViewModel> GetAllExperiences()
        {
            Response<ExperienceViewModel> response = new Response<ExperienceViewModel>();
            try
            {
                var experiencesFromRepo = _context.Experience.ToList();
                var experienceViewModels = experiencesFromRepo.Select(experience => new ExperienceViewModel
                {
                    Id = experience.Id,
                    Position = experience.Position,
                    Company = experience.Company,
                    UpdatedBy = experience.UpdatedBy,
                    CreatedBy = experience.CreatedBy

                }).ToList();

                response.DataList = experienceViewModels;
                response.Code = StatusCodes.Status200OK;
            }
            catch (Exception)
            {
                response.Code = StatusCodes.Status400BadRequest;
            }

            return response;
        }

        public Response<ExperienceViewModel> GetExperienceById(Guid id)
        {
            Response<ExperienceViewModel> response = new Response<ExperienceViewModel>();
            try
            {
                var experience = _context.Experience.FirstOrDefault(e => e.Id == id);

                if (experience == null)
                {
                    response.Message = "Không tìm thấy kinh nghiệm";
                    response.Code = StatusCodes.Status404NotFound;
                }
                else
                {
                    var experienceViewModel = new ExperienceViewModel
                    {
                        Id = experience.Id,
                        Position = experience.Position,
                        Company = experience.Company
                    };

                    response.Data = experienceViewModel;
                    response.Message = "Lấy thành công";
                    response.Code = StatusCodes.Status200OK;
                }
            }
            catch (Exception)
            {
                response.Code = StatusCodes.Status400BadRequest;
            }

            return response;
        }
    }
}
