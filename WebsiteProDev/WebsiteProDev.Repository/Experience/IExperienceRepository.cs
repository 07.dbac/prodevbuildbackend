﻿using WebsiteProDev.DataLayer.Entity;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Experience;

namespace WebsiteProDev.Repository.Experience
{
    public interface IExperienceRepository
    {
            Response<string> CreateExperience(ExperienceModel experience);

            Response<string> UpdateExperience(ExperienceModel experience);

            Response<string> DeleteExperienceById(Guid id);

            Response<ExperienceViewModel> GetAllExperiences();

            Response<ExperienceViewModel> GetExperienceById(Guid Id); 
    }
}
