﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebsiteProDev.DataLayer.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Experience",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("f3e1dcb6-e725-45f9-a37d-56b9c8995f04")),
                    Position = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Company = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1555)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1612)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Experience", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Level",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("6d386f1b-a510-46c3-9026-70eba9701b1e")),
                    LevelName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Score = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1967)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(2048)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Level", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("9fe1b62e-5931-4945-8c1a-a61c1ffbc68d")),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MidName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuizLanguage",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("921ba1d3-14b0-4804-9e62-296f30b37ba0")),
                    QuizLanguageName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(8506)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(8585)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizLanguage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("7a7552f1-5e18-47c8-a579-21b7c4cae4e1")),
                    RoleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(9508)),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(9567)),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLogin",
                columns: table => new
                {
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogin", x => new { x.UserId, x.ProviderKey });
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId });
                });

            migrationBuilder.CreateTable(
                name: "UserToken",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => new { x.UserId, x.LoginProvider });
                });

            migrationBuilder.CreateTable(
                name: "PersonExperience",
                columns: table => new
                {
                    PersonId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("1c04d4ce-8678-4a4d-9f78-cc149cf08137")),
                    ExperienceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("a36711f3-b9b6-4295-9b82-3cc5f4c712a5"))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonExperience", x => new { x.PersonId, x.ExperienceId });
                    table.ForeignKey(
                        name: "FK_PersonExperience_Experience_ExperienceId",
                        column: x => x.ExperienceId,
                        principalTable: "Experience",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonExperience_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("03637e59-9a20-4738-afe6-c17602747389")),
                    CourseName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CourseDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ApprovedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    ApprovedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    QuizLanguageId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LanguageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ImageData = table.Column<byte[]>(type: "varbinary(MAX)", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(876)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(946)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Course_QuizLanguage_QuizLanguageId",
                        column: x => x.QuizLanguageId,
                        principalTable: "QuizLanguage",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Quiz",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("f17a062f-36f9-46a2-9caf-5b40a3ed8aa4")),
                    QuizName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    QuizDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QuizSlug = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Score = table.Column<double>(type: "float", nullable: false),
                    QuizLanguageId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(8043)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(8130)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quiz", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quiz_QuizLanguage_QuizLanguageId",
                        column: x => x.QuizLanguageId,
                        principalTable: "QuizLanguage",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("7f13bd19-b2ab-4ba2-a803-c16daac00e98")),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PersonId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 585, DateTimeKind.Utc).AddTicks(1624)),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 585, DateTimeKind.Utc).AddTicks(1681)),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_User_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "QuizQuestion",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("2a035b90-c7e8-4147-b6b6-1fcbe3ed9cb8")),
                    Status = table.Column<int>(type: "int", nullable: false),
                    QuizId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LevelsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(8995)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(9088)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizQuestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizQuestion_Level_LevelsId",
                        column: x => x.LevelsId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizQuestion_Quiz_QuizId",
                        column: x => x.QuizId,
                        principalTable: "Quiz",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Take",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("e435f48d-8124-481d-bbb0-aad301120d0c")),
                    QuizId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Score = table.Column<double>(type: "float", nullable: false),
                    StartedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 585, DateTimeKind.Utc).AddTicks(1151)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 585, DateTimeKind.Utc).AddTicks(1238)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Take", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Take_Quiz_QuizId",
                        column: x => x.QuizId,
                        principalTable: "Quiz",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Take_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserCourse",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CourseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCourse", x => new { x.UserId, x.CourseId });
                    table.ForeignKey(
                        name: "FK_UserCourse_Course_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserCourse_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuizAnswer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("584558d1-797a-47d8-aace-55565499d25a")),
                    Correct = table.Column<bool>(type: "bit", nullable: false),
                    QuizQuestionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(7400)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(7461)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizAnswer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizAnswer_QuizQuestion_QuizQuestionId",
                        column: x => x.QuizQuestionId,
                        principalTable: "QuizQuestion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("f0422562-f161-4726-9d2f-cdafcd07f742")),
                    DateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CourseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(2384)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(2443)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payment_UserCourse_UserId_CourseId",
                        columns: x => new { x.UserId, x.CourseId },
                        principalTable: "UserCourse",
                        principalColumns: new[] { "UserId", "CourseId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TakeAnswer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("0356f723-6cf3-4c81-babb-30c54e25fe2c")),
                    TakeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    QuizAnswerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(9990)),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValue: new DateTime(2023, 6, 9, 17, 32, 54, 585, DateTimeKind.Utc).AddTicks(50)),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TakeAnswer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TakeAnswer_QuizAnswer_QuizAnswerId",
                        column: x => x.QuizAnswerId,
                        principalTable: "QuizAnswer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TakeAnswer_Take_TakeId",
                        column: x => x.TakeId,
                        principalTable: "Take",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Course",
                columns: new[] { "Id", "ApprovedAt", "ApprovedBy", "CourseDescription", "CourseName", "CreatedBy", "ImageData", "LanguageId", "Price", "QuizLanguageId", "Status", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("150db41c-8766-4dce-9467-d236a8489d02"), new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1080), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Learning C# basic", "C#", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null, new Guid("00000000-0000-0000-0000-000000000000"), 100m, null, true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("160db41c-8766-4dce-9467-d236a8489d02"), new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1085), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Learning C++ basic", "C++", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null, new Guid("00000000-0000-0000-0000-000000000000"), 100m, null, true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("170db41c-8766-4dce-9467-d236a8489d02"), new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1087), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Learning Python basic", "Python", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null, new Guid("00000000-0000-0000-0000-000000000000"), 100m, null, true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("180db41c-8766-4dce-9467-d236a8489d02"), new DateTime(2023, 6, 9, 17, 32, 54, 584, DateTimeKind.Utc).AddTicks(1090), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Learning Ruby basic", "Ruby", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null, new Guid("00000000-0000-0000-0000-000000000000"), 100m, null, true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "Experience",
                columns: new[] { "Id", "Company", "CreatedBy", "Position", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("e0df41f3-3195-48ef-8d05-ca564277d548"), "ABC Technologies", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Software Engineer", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("e0df41f3-3195-48ef-8d05-ca564277d549"), "XYZ Corporation", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Product Manager", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("e0df41f3-3195-48ef-8d05-ca564277d550"), "123 Marketing Agency", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Marketing Specialist", new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "Level",
                columns: new[] { "Id", "CreatedBy", "LevelName", "Score", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("353ab79b-3195-48ef-8d05-ca564277d548"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Mid-level", 8.2m, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("52ba17a0-3195-48ef-8d05-ca564277d548"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Senior", 9.5m, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("d3affd1e-3195-48ef-8d05-ca564277d548"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Junior", 7.1m, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "Person",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "FirstName", "IsDeleted", "LastName", "MidName", "Status", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { new Guid("220db41c-8766-4dce-9467-d236a8489d22"), null, null, "Vu", null, "Dung", "Van", false, null, null },
                    { new Guid("230db41c-8766-4dce-9467-d236a8489d23"), null, null, "Do", null, "Bac", "Doan", false, null, null },
                    { new Guid("240db41c-8766-4dce-9467-d236a8489d24"), null, null, "Nguyen", null, "Hau", "Dinh", false, null, null }
                });

            migrationBuilder.InsertData(
                table: "QuizLanguage",
                columns: new[] { "Id", "CreatedBy", "QuizLanguageName", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("aec1c31d-2381-4203-98e6-2223d82aef54"), null, "C#", null },
                    { new Guid("aec1c31d-2381-4203-98e6-2223d82aef55"), null, "Java", null }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy", "CreatedDate", "Name", "NormalizedName", "RoleName", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "49d0d7b0-370d-4bca-919e-c224554a25ff", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9662), null, null, "User", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9662) },
                    { new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd0"), "1ca4ec3a-a05b-4cef-b9da-bfe9a12dfc52", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9643), null, null, "Admin", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9658) },
                    { new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd1"), "63bd8438-b5d0-4150-bb25-4eab8f5df992", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9667), null, null, "Manager", "AdminUser", new DateTime(2023, 6, 10, 0, 32, 54, 584, DateTimeKind.Local).AddTicks(9668) }
                });

            migrationBuilder.InsertData(
                table: "PersonExperience",
                columns: new[] { "ExperienceId", "PersonId" },
                values: new object[,]
                {
                    { new Guid("e0df41f3-3195-48ef-8d05-ca564277d548"), new Guid("220db41c-8766-4dce-9467-d236a8489d22") },
                    { new Guid("e0df41f3-3195-48ef-8d05-ca564277d548"), new Guid("230db41c-8766-4dce-9467-d236a8489d23") }
                });

            migrationBuilder.InsertData(
                table: "Quiz",
                columns: new[] { "Id", "CreatedBy", "QuizDescription", "QuizLanguageId", "QuizName", "QuizSlug", "Score", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("d3affd1e-3195-48ef-8d05-ca564277d656"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Explore the world of science", new Guid("aec1c31d-2381-4203-98e6-2223d82aef55"), "Science Quiz", "science-quiz", 8.1999999999999993, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), "Test your math skills", new Guid("aec1c31d-2381-4203-98e6-2223d82aef54"), "Math Quiz", "math-quiz", 9.5, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PersonId", "PhoneNumber", "PhoneNumberConfirmed", "RoleId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), 0, "8dcebacd-aacc-4b77-93c2-bed0ac75a644", "dodoanbac@gmail.com", false, false, null, null, null, "123123", new Guid("220db41c-8766-4dce-9467-d236a8489d22"), null, false, new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd0"), null, false, "doanbac07" },
                    { new Guid("2d002d4c-1201-47ff-8af7-981e437621a8"), 0, "2dde2310-18c3-4c0f-92c1-c3cd522f4c14", "dodoanbac@gmail.com", false, false, null, null, null, "123321", new Guid("240db41c-8766-4dce-9467-d236a8489d24"), null, false, new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd1"), null, false, "doanbac07" },
                    { new Guid("7f70269b-adb8-4954-8bf2-2c419017fd9e"), 0, "5760b5f1-c021-4eda-b364-afb082fc0ff9", "dodoanbac@gmail.com", false, false, null, null, null, "123123", new Guid("230db41c-8766-4dce-9467-d236a8489d23"), null, false, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null, false, "doanbac07" }
                });

            migrationBuilder.InsertData(
                table: "QuizQuestion",
                columns: new[] { "Id", "CreatedBy", "LevelsId", "QuizId", "Status", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b46d"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("52ba17a0-3195-48ef-8d05-ca564277d548"), new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), 3, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b50a"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("52ba17a0-3195-48ef-8d05-ca564277d548"), new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), 2, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b89b"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("52ba17a0-3195-48ef-8d05-ca564277d548"), new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), 1, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "Take",
                columns: new[] { "Id", "CreatedBy", "FinishedAt", "QuizId", "Score", "StartedAt", "Status", "UpdatedBy", "UserId" },
                values: new object[,]
                {
                    { new Guid("e940cef9-1911-47ea-874f-b2acedd6835d"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new DateTime(2023, 6, 10, 1, 32, 54, 585, DateTimeKind.Local).AddTicks(1297), new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), 9.0999999999999996, new DateTime(2023, 6, 10, 0, 32, 54, 585, DateTimeKind.Local).AddTicks(1296), 1, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("e940cef9-1911-47ea-874f-b2acedd6836a"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new DateTime(2023, 6, 10, 1, 17, 54, 585, DateTimeKind.Local).AddTicks(1291), new Guid("d3affd1e-3195-48ef-8d05-ca564277d656"), 7.2000000000000002, new DateTime(2023, 6, 10, 0, 32, 54, 585, DateTimeKind.Local).AddTicks(1290), 2, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    { new Guid("e940cef9-1911-47ea-874f-b2acedd6837b"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new DateTime(2023, 6, 10, 1, 2, 54, 585, DateTimeKind.Local).AddTicks(1282), new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), 8.5, new DateTime(2023, 6, 10, 0, 32, 54, 585, DateTimeKind.Local).AddTicks(1280), 1, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") }
                });

            migrationBuilder.InsertData(
                table: "UserCourse",
                columns: new[] { "CourseId", "UserId", "Date" },
                values: new object[,]
                {
                    { new Guid("150db41c-8766-4dce-9467-d236a8489d02"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), null },
                    { new Guid("150db41c-8766-4dce-9467-d236a8489d02"), new Guid("7f70269b-adb8-4954-8bf2-2c419017fd9e"), null }
                });

            migrationBuilder.InsertData(
                table: "Payment",
                columns: new[] { "Id", "CourseId", "CreatedBy", "DateTime", "Status", "UpdatedBy", "UserId" },
                values: new object[] { new Guid("e0df41f4-3195-48ef-8d05-ca564277d548"), new Guid("150db41c-8766-4dce-9467-d236a8489d02"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new DateTime(2023, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") });

            migrationBuilder.InsertData(
                table: "QuizAnswer",
                columns: new[] { "Id", "Correct", "CreatedBy", "QuizQuestionId", "UpdatedBy" },
                values: new object[] { new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0abc"), false, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b46d"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") });

            migrationBuilder.InsertData(
                table: "QuizAnswer",
                columns: new[] { "Id", "Correct", "CreatedBy", "QuizQuestionId", "UpdatedBy" },
                values: new object[] { new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), true, new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b89b"), new Guid("05c8c36a-89f5-43eb-8f38-976637f85489") });

            migrationBuilder.InsertData(
                table: "TakeAnswer",
                columns: new[] { "Id", "CreatedBy", "QuizAnswerId", "TakeId", "UpdatedBy" },
                values: new object[] { new Guid("9fcadbfd-28e7-4e5b-9e3d-0e33538187c8"), null, new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), new Guid("e940cef9-1911-47ea-874f-b2acedd6837b"), null });

            migrationBuilder.InsertData(
                table: "TakeAnswer",
                columns: new[] { "Id", "CreatedBy", "QuizAnswerId", "TakeId", "UpdatedBy" },
                values: new object[] { new Guid("9fcadbfd-28e7-4e5b-9e3d-0e33538187c9"), null, new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), new Guid("e940cef9-1911-47ea-874f-b2acedd6837b"), null });

            migrationBuilder.CreateIndex(
                name: "IX_Course_QuizLanguageId",
                table: "Course",
                column: "QuizLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserId_CourseId",
                table: "Payment",
                columns: new[] { "UserId", "CourseId" });

            migrationBuilder.CreateIndex(
                name: "IX_PersonExperience_ExperienceId",
                table: "PersonExperience",
                column: "ExperienceId");

            migrationBuilder.CreateIndex(
                name: "IX_Quiz_QuizLanguageId",
                table: "Quiz",
                column: "QuizLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizAnswer_QuizQuestionId",
                table: "QuizAnswer",
                column: "QuizQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizQuestion_LevelsId",
                table: "QuizQuestion",
                column: "LevelsId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizQuestion_QuizId",
                table: "QuizQuestion",
                column: "QuizId");

            migrationBuilder.CreateIndex(
                name: "IX_Take_QuizId",
                table: "Take",
                column: "QuizId");

            migrationBuilder.CreateIndex(
                name: "IX_Take_UserId",
                table: "Take",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TakeAnswer_QuizAnswerId",
                table: "TakeAnswer",
                column: "QuizAnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_TakeAnswer_TakeId",
                table: "TakeAnswer",
                column: "TakeId");

            migrationBuilder.CreateIndex(
                name: "IX_User_PersonId",
                table: "User",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleId",
                table: "User",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCourse_CourseId",
                table: "UserCourse",
                column: "CourseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Payment");

            migrationBuilder.DropTable(
                name: "PersonExperience");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "TakeAnswer");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogin");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "UserToken");

            migrationBuilder.DropTable(
                name: "UserCourse");

            migrationBuilder.DropTable(
                name: "Experience");

            migrationBuilder.DropTable(
                name: "QuizAnswer");

            migrationBuilder.DropTable(
                name: "Take");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "QuizQuestion");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Level");

            migrationBuilder.DropTable(
                name: "Quiz");

            migrationBuilder.DropTable(
                name: "Person");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "QuizLanguage");
        }
    }
}
