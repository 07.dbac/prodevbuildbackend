﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebsiteProDev.DataLayer.Config;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataAccess.Context
{
    public class ProDevDbContext : IdentityDbContext<User, Role, Guid>
    {
        public ProDevDbContext(DbContextOptions<ProDevDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CourseConfig());
            modelBuilder.ApplyConfiguration(new ExperienceConfig());
            modelBuilder.ApplyConfiguration(new LevelConfig());
            modelBuilder.ApplyConfiguration(new PaymentConfig());
            modelBuilder.ApplyConfiguration(new PersonConfig());
            modelBuilder.ApplyConfiguration(new PersonExperienceConfig());
            modelBuilder.ApplyConfiguration(new QuizAnswerConfig());
            modelBuilder.ApplyConfiguration(new QuizConfig());
            modelBuilder.ApplyConfiguration(new QuizLanguageConfig());
            modelBuilder.ApplyConfiguration(new QuizQuestionConfig());
            modelBuilder.ApplyConfiguration(new RoleConfig());
            modelBuilder.ApplyConfiguration(new TakeAnswerConfig());
            modelBuilder.ApplyConfiguration(new TakeConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new UserCourseConfig());
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogin").HasKey(x => new { x.UserId, x.ProviderKey });
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("UserRole").HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("UserToken").HasKey(x => new { x.UserId, x.LoginProvider });
        }

        public virtual DbSet<Level> Level { get; set; }
        public virtual DbSet<Quiz> Quiz { get; set; }
        public virtual DbSet<QuizAnswer> QuizAnswer { get; set; }
        public virtual DbSet<QuizLanguage> QuizLanguage { get; set; }
        public virtual DbSet<QuizQuestion> QuizQuestion { get; set; }
        public virtual DbSet<Take> Take { get; set; }
        public virtual DbSet<TakeAnswer> TakesAnswer { get; set; }
        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<Experience> Experience { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<UserCourse> UserCourse { get; set; }
        public virtual DbSet<PersonExperience> PersonExperience { get; set; }
        public virtual DbSet<User> User { get; set; }
    }
}