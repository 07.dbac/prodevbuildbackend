﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            // seed data
            builder
                .HasData
            (
               new User { Id = new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), RoleId = Guid.Parse("d8e53017-79db-4b8e-aa93-a2c7335bcdd0"), PersonId = Guid.Parse("220db41c-8766-4dce-9467-d236a8489d22"), Email = "dodoanbac@gmail.com", UserName = "doanbac07", PasswordHash = "123123" },
               new User { Id = new Guid("7f70269b-adb8-4954-8bf2-2c419017fd9e"), RoleId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), PersonId = Guid.Parse("230db41c-8766-4dce-9467-d236a8489d23"), Email = "dodoanbac@gmail.com", UserName = "doanbac07", PasswordHash = "123123" },
               new User { Id = new Guid("2d002d4c-1201-47ff-8af7-981e437621a8"), RoleId = Guid.Parse("d8e53017-79db-4b8e-aa93-a2c7335bcdd1"), PersonId = Guid.Parse("240db41c-8766-4dce-9467-d236a8489d24"), Email = "dodoanbac@gmail.com", UserName = "doanbac07", PasswordHash = "123321" }
            );
        }
    }
}