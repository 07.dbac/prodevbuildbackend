﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class PersonExperienceConfig : IEntityTypeConfiguration<PersonExperience>
    {
        public void Configure(EntityTypeBuilder<PersonExperience> builder)
        {
            builder.ToTable(nameof(PersonExperience));
            builder.Property(x => x.PersonId).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.ExperienceId).HasDefaultValue(Guid.NewGuid());
            builder.HasKey(ue => new { ue.PersonId, ue.ExperienceId });
            // Seed data
            builder
                .HasData
                (
                new PersonExperience { PersonId = Guid.Parse("220db41c-8766-4dce-9467-d236a8489d22"), ExperienceId = Guid.Parse("e0df41f3-3195-48ef-8d05-ca564277d548") },
                new PersonExperience { PersonId = Guid.Parse("230db41c-8766-4dce-9467-d236a8489d23"), ExperienceId = Guid.Parse("e0df41f3-3195-48ef-8d05-ca564277d548") }
                );
        }
    }
}