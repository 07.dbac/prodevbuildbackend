﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class TakeAnswerConfig : IEntityTypeConfiguration<TakeAnswer>
    {
        public void Configure(EntityTypeBuilder<TakeAnswer> builder)
        {
            builder.ToTable(nameof(TakeAnswer));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            builder
           .HasOne(ta => ta.Take)
           .WithMany(t => t.TakeAnswers)
           .OnDelete(DeleteBehavior.Restrict);

            // Seed data
            builder
                .HasData
               (
                   new TakeAnswer { Id = new Guid("9fcadbfd-28e7-4e5b-9e3d-0e33538187c8"), QuizAnswerId = Guid.Parse("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), TakeId = Guid.Parse("e940cef9-1911-47ea-874f-b2acedd6837b") },
                   new TakeAnswer { Id = new Guid("9fcadbfd-28e7-4e5b-9e3d-0e33538187c9"), QuizAnswerId = Guid.Parse("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), TakeId = Guid.Parse("e940cef9-1911-47ea-874f-b2acedd6837b") }
               );
        }
    }
}