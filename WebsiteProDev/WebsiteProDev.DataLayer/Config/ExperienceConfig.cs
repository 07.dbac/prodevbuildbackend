﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class ExperienceConfig : IEntityTypeConfiguration<Experience>
    {
        public void Configure(EntityTypeBuilder<Experience> builder)
        {
            builder.ToTable(nameof(Experience));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            /// seed data
            builder
            .HasData(
            new Experience { Id = new Guid("e0df41f3-3195-48ef-8d05-ca564277d548"), Position = "Software Engineer", Company = "ABC Technologies", CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
            new Experience { Id = new Guid("e0df41f3-3195-48ef-8d05-ca564277d549"), Position = "Product Manager", Company = "XYZ Corporation", CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
            new Experience { Id = new Guid("e0df41f3-3195-48ef-8d05-ca564277d550"), Position = "Marketing Specialist", Company = "123 Marketing Agency", CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") }
        );
        }
    }
}