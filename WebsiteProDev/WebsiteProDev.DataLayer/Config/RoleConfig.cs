﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class RoleConfig : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable(nameof(Role));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            //Seed data
            builder
                  .HasData(
                      new Role { Id = new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd0"), RoleName = "Admin", CreatedBy = "AdminUser", CreatedDate = DateTime.Now, UpdatedBy = "AdminUser", UpdatedDate = DateTime.Now },
                      new Role { Id = new Guid("05c8c36a-89f5-43eb-8f38-976637f85489"), RoleName = "User", CreatedBy = "AdminUser", CreatedDate = DateTime.Now, UpdatedBy = "AdminUser", UpdatedDate = DateTime.Now },
                      new Role { Id = new Guid("d8e53017-79db-4b8e-aa93-a2c7335bcdd1"), RoleName = "Manager", CreatedBy = "AdminUser", CreatedDate = DateTime.Now, UpdatedBy = "AdminUser", UpdatedDate = DateTime.Now }
                  );
        }
    }
}