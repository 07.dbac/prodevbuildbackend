﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class PaymentConfig : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.ToTable(nameof(Payment));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);
            builder
            .HasOne(p => p.UserCourse)
            .WithMany(uc => uc.Payments)
            .HasForeignKey(p => new { p.UserId, p.CourseId });

            //record Payment
            builder
                .HasData(
                    new Payment { Id = new Guid("e0df41f4-3195-48ef-8d05-ca564277d548"), UserId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), CourseId = Guid.Parse("150db41c-8766-4dce-9467-d236a8489d02"), DateTime = new DateTime(2023, 1, 1), Status = true, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") }
                 );
        }
    }
}