﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class QuizConfig : IEntityTypeConfiguration<Quiz>
    {
        public void Configure(EntityTypeBuilder<Quiz> builder)
        {
            builder.ToTable(nameof(Quiz));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            // Seed data
            builder
              .HasData
              (
                  new Quiz { Id = new Guid("d3affd1e-3195-48ef-8d05-ca564277d666"), QuizName = "Math Quiz", QuizDescription = "Test your math skills", QuizSlug = "math-quiz", Score = 9.5, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizLanguageId = Guid.Parse("aec1c31d-2381-4203-98e6-2223d82aef54") },
                  new Quiz { Id = new Guid("d3affd1e-3195-48ef-8d05-ca564277d656"), QuizName = "Science Quiz", QuizDescription = "Explore the world of science", QuizSlug = "science-quiz", Score = 8.2, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizLanguageId = Guid.Parse("aec1c31d-2381-4203-98e6-2223d82aef55") }
              );
        }
    }
}