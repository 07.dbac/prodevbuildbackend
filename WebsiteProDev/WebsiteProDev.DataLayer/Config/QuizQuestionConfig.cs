﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class QuizQuestionConfig : IEntityTypeConfiguration<QuizQuestion>
    {
        public void Configure(EntityTypeBuilder<QuizQuestion> builder)
        {
            builder.ToTable(nameof(QuizQuestion));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            //Record Quiz Question
            builder
                .HasData(
                   new QuizQuestion { Id = new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b89b"), Status = 1, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d666"), LevelsId = Guid.Parse("52ba17a0-3195-48ef-8d05-ca564277d548") },
                   new QuizQuestion { Id = new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b50a"), Status = 2, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d666"), LevelsId = Guid.Parse("52ba17a0-3195-48ef-8d05-ca564277d548") },
                   new QuizQuestion { Id = new Guid("09ae0899-1e3f-4b97-be73-d2f8b5b6b46d"), Status = 3, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d666"), LevelsId = Guid.Parse("52ba17a0-3195-48ef-8d05-ca564277d548") }
                );
        }
    }
}