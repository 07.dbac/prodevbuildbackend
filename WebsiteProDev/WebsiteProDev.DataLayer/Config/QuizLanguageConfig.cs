﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class QuizLanguageConfig : IEntityTypeConfiguration<QuizLanguage>
    {
        public void Configure(EntityTypeBuilder<QuizLanguage> builder)
        {
            builder.ToTable(nameof(QuizLanguage));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            // Seed data
            builder
                .HasData
                (
                new QuizLanguage { Id = new Guid("aec1c31d-2381-4203-98e6-2223d82aef54"), QuizLanguageName = "C#" },
                new QuizLanguage { Id = new Guid("aec1c31d-2381-4203-98e6-2223d82aef55"), QuizLanguageName = "Java" }
                );
        }
    }
}