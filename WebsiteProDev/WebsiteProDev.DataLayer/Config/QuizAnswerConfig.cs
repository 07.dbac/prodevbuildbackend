﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class QuizAnswerConfig : IEntityTypeConfiguration<QuizAnswer>
    {
        public void Configure(EntityTypeBuilder<QuizAnswer> builder)
        {
            builder.ToTable(nameof(QuizAnswer));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);
            // Seed Data
            builder
                .HasData
                (
                    new QuizAnswer { Id = new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0bec"), QuizQuestionId = Guid.Parse("09ae0899-1e3f-4b97-be73-d2f8b5b6b89b"), Correct = true, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
                    new QuizAnswer { Id = new Guid("0d237e53-997c-4b9f-9cef-581d1e3f0abc"), QuizQuestionId = Guid.Parse("09ae0899-1e3f-4b97-be73-d2f8b5b6b46d"), Correct = false, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") }
                );
        }
    }
}