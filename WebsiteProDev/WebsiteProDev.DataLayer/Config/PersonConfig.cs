﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Reflection.Emit;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class PersonConfig : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.ToTable(nameof(Person));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());

            // Seed data
           builder
   .HasData
   (
       new Person { Id = new Guid("220db41c-8766-4dce-9467-d236a8489d22"), FirstName = "Vu", MidName = "Van", LastName = "Dung" },
       new Person { Id = new Guid("230db41c-8766-4dce-9467-d236a8489d23"), FirstName = "Do", MidName = "Doan", LastName = "Bac" },
       new Person { Id = new Guid("240db41c-8766-4dce-9467-d236a8489d24"), FirstName = "Nguyen", MidName = "Dinh", LastName = "Hau" }
   );
        }
    }
}