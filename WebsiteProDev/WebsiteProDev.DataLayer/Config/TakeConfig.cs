﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class TakeConfig : IEntityTypeConfiguration<Take>
    {
        public void Configure(EntityTypeBuilder<Take> builder)
        {
            builder.ToTable(nameof(Take));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);

            // Seed data
            builder
           .HasData(
               new Take { Id = new Guid("e940cef9-1911-47ea-874f-b2acedd6837b"), Status = 1, Score = 8.5, StartedAt = DateTime.Now, FinishedAt = DateTime.Now.AddMinutes(30),CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d666"), UserId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
               new Take { Id = new Guid("e940cef9-1911-47ea-874f-b2acedd6836a"), Status = 2, Score = 7.2, StartedAt = DateTime.Now, FinishedAt = DateTime.Now.AddMinutes(45),CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d656"), UserId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
               new Take { Id = new Guid("e940cef9-1911-47ea-874f-b2acedd6835d"), Status = 1, Score = 9.1, StartedAt = DateTime.Now, FinishedAt = DateTime.Now.AddMinutes(60),CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), QuizId = Guid.Parse("d3affd1e-3195-48ef-8d05-ca564277d666"), UserId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") }
           );
        }
    }
}