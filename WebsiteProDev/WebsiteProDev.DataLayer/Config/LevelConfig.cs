﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class LevelConfig : IEntityTypeConfiguration<Level>
    {
        public void Configure(EntityTypeBuilder<Level> builder)
        {
            builder.ToTable(nameof(Level));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);
            builder
                .HasData(
            new Level { Id = new Guid("52ba17a0-3195-48ef-8d05-ca564277d548"), LevelName = "Senior", Score = 9.5m, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
            new Level { Id = new Guid("353ab79b-3195-48ef-8d05-ca564277d548"), LevelName = "Mid-level", Score = 8.2m, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
            new Level { Id = new Guid("d3affd1e-3195-48ef-8d05-ca564277d548"), LevelName = "Junior", Score = 7.1m, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") }
            );
        }
    }
}