﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class CourseConfig : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable(nameof(Course));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasDefaultValue(Guid.NewGuid());
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.CreatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x => x.UpdatedDate).HasDefaultValue(DateTime.UtcNow);
            builder.Property(e => e.ImageData).HasColumnType("varbinary(MAX)");

            // Seed data
            builder
                 .HasData
                 (
                     new Course { Id = new Guid("150db41c-8766-4dce-9467-d236a8489d02"), CourseName = "C#", CourseDescription = "Learning C# basic", Price = 100, ApprovedBy = Guid.Parse("05C8C36A-89F5-43EB-8F38-976637F85489"), Status = true, ApprovedAt = DateTime.UtcNow, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489")},
                     new Course { Id = new Guid("160db41c-8766-4dce-9467-d236a8489d02"), CourseName = "C++", CourseDescription = "Learning C++ basic", Price = 100, ApprovedBy = Guid.Parse("05C8C36A-89F5-43EB-8F38-976637F85489"), Status = true, ApprovedAt = DateTime.UtcNow, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
                     new Course { Id = new Guid("170db41c-8766-4dce-9467-d236a8489d02"), CourseName = "Python", CourseDescription = "Learning Python basic", Price = 100, ApprovedBy = Guid.Parse("05C8C36A-89F5-43EB-8F38-976637F85489"), Status = true, ApprovedAt = DateTime.UtcNow, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489") },
                     new Course { Id = new Guid("180db41c-8766-4dce-9467-d236a8489d02"), CourseName = "Ruby", CourseDescription = "Learning Ruby basic", Price = 100, ApprovedBy = Guid.Parse("05C8C36A-89F5-43EB-8F38-976637F85489"), Status = true, ApprovedAt = DateTime.UtcNow, CreatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), UpdatedBy = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489")}
                 );
        }
    }
}