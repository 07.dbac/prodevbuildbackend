﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebsiteProDev.DataLayer.Entity;

namespace WebsiteProDev.DataLayer.Config
{
    public class UserCourseConfig : IEntityTypeConfiguration<UserCourse>
    {
        public void Configure(EntityTypeBuilder<UserCourse> builder)
        {
            builder.HasKey(uc => new { uc.UserId, uc.CourseId });

            // Seed data
            builder.HasData
                (
                  new UserCourse { UserId = Guid.Parse("05c8c36a-89f5-43eb-8f38-976637f85489"), CourseId = Guid.Parse("150db41c-8766-4dce-9467-d236a8489d02") },
                  new UserCourse { UserId = Guid.Parse("7f70269b-adb8-4954-8bf2-2c419017fd9e"), CourseId = Guid.Parse("150db41c-8766-4dce-9467-d236a8489d02") }
                );
        }
    }
}