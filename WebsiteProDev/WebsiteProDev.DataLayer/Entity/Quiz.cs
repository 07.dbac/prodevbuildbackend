﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Quiz : EntityBase
    {
        public string QuizName { get; set; }
        public string? QuizDescription { get; set; }
        public string? QuizSlug { get; set; }
        public double Score { get; set; }
        public IList<QuizQuestion>? QuizQuestions { get; set; }
        public QuizLanguage? QuizLanguage { get; set; }
        public Guid? QuizLanguageId { get; set; }
        public IList<Take>? Takes { get; set; }
    }
}