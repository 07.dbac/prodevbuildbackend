﻿using Microsoft.AspNetCore.Identity;

namespace WebsiteProDev.DataLayer.Entity
{
    public class User : IdentityUser<Guid>
    {
        public bool IsDeleted { get; set; }
        public Guid? RoleId { get; set; }
        public Guid? PersonId { get; set; }
        public Person? Person { get; set; }
        public Role? Role { get; set; }
        public IList<UserCourse>? UserCourses { get; set; }
        public IList<Take>? Takes { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}