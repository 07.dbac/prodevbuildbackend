﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Level : EntityBase
    {
        public string LevelName { get; set; }
        public decimal Score { get; set; }
        public IList<QuizQuestion>? QuizQuestions { get; set; }
    }
}