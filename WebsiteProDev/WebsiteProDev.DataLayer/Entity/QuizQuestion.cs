﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class QuizQuestion : EntityBase
    {
        public int Status { get; set; }

        public Guid QuizId { get; set; }
        public Quiz? Quiz { get; set; }
        public IList<QuizAnswer>? QuizAnswers { get; set; }
        public Guid LevelsId { get; set; }

        public Level? Levels { get; set; }
    }
}