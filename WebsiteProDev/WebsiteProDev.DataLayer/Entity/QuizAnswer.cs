﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class QuizAnswer : EntityBase
    {
        public bool Correct { get; set; }
        public Guid QuizQuestionId { get; set; }
        public QuizQuestion? QuizQuestion { get; set; }
        public IList<TakeAnswer>? TakeAnswers { get; set; }
    }
}