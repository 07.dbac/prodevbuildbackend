﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Experience : EntityBase
    {
        public string? Position { get; set; }
        public string? Company { get; set; }
        public IList<PersonExperience>? PersonExperiences { get; set; }
    }
}