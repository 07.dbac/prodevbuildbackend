﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class PersonExperience
    {
        public Person Person { get; set; }
        public Experience Experience { get; set; }
        public Guid? PersonId { get; set; }
        public Guid? ExperienceId { get; set; }
    }
}