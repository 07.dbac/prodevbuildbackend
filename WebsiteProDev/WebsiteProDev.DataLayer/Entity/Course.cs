﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Course : EntityBase
    {
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public Decimal? Price { get; set; }
        public Guid ApprovedBy { get; set; }
        public bool Status { get; set; }
        public DateTime ApprovedAt { get; set; }
        public IList<UserCourse>? UserCourses { get; set; }
        public QuizLanguage? QuizLanguage { get; set; }
        public Guid LanguageId { get; set; }
        public byte[]? ImageData { get; set; }
    }
}