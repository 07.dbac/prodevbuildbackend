﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Person : EntityBase
    {
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
        public IList<User>? Users { get; set; }
        public IList<PersonExperience>? PersonExperiences { get; set; }
    }
}