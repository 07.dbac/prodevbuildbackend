﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class TakeAnswer : EntityBase
    {
        public Guid TakeId { get; set; }
        public Guid QuizAnswerId { get; set; }
        public QuizAnswer? QuizAnswer { get; set; }
        public Take? Take { get; set; }
    }
}