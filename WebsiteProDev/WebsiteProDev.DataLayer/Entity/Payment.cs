﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Payment : EntityBase
    {
        public DateTime? DateTime { get; set; }
        public bool Status { get; set; }
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public UserCourse UserCourse { get; set; }
    }
}