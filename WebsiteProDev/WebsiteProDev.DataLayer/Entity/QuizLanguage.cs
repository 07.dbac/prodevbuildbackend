﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class QuizLanguage : EntityBase
    {
        public string QuizLanguageName { get; set; }
        public IList<Quiz>? Quizzes { get; set; }
    }
}