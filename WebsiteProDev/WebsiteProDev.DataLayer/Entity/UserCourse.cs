﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class UserCourse
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime? Date { get; set; }
        public User? User { get; set; }
        public Course? Course { get; set; }
        public IList<Payment>? Payments { get; set; }
    }
}