﻿using Microsoft.AspNetCore.Identity;

namespace WebsiteProDev.DataLayer.Entity
{
    public class Role : IdentityRole<Guid>
    {
        public string RoleName { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}