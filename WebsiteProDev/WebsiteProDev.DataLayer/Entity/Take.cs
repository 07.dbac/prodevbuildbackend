﻿namespace WebsiteProDev.DataLayer.Entity
{
    public class Take : EntityBase
    {
        public Guid QuizId { get; set; }
        public Quiz Quiz { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        public double Score { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public IList<TakeAnswer>? TakeAnswers { get; set; }
    }
}