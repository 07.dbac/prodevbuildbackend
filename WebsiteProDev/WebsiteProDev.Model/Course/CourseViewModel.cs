﻿namespace WebsiteProDev.Model.Course
{
    public class CourseViewModel
    {
        public Guid Id { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public Decimal? Price { get; set; }
        public Guid? ApprovedBy { get; set; }
        public bool Status { get; set; }
        //  public DateTime ApprovedAt { get; set; }
        public Guid LanguageId { get; set; }
        public byte[] ImageData { get; set; }
        // public Guid? CreatedBy { get; set; }
        // public DateTime CreatedDate { get; set; }
        // public Guid UpdatedBy { get; set; }
        // public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }

    }
}