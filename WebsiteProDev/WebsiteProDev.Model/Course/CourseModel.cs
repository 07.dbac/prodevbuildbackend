﻿using Microsoft.AspNetCore.Http;

namespace WebsiteProDev.Model.Course
{
    public class CourseModel : ModelBase
    {
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public Decimal? Price { get; set; }
        public Guid? ApprovedBy { get; set; }
        public bool Status { get; set; }
        public DateTime? ApprovedAt { get; set; }
        public Guid LanguageId { get; set; }
        public IFormFile ImageData { get; set; }
    }
}