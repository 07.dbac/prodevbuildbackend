﻿namespace WebsiteProDev.Model.User
{
    public class UserModelView
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public Guid RoleId { get; set; }
        public Guid PersonId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}