﻿namespace WebsiteProDev.Model.Quiz
{
    public class QuizModel : ModelBase
    {
        public string QuizName { get; set; }
        public string? QuizDescription { get; set; }
        public string? QuizSlug { get; set; }
        public double Score { get; set; }
        public Guid QuizLanguageId { get; set; }
    }
}