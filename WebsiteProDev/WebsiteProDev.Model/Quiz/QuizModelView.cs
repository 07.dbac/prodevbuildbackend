﻿namespace WebsiteProDev.Model.Quiz
{
    public class QuizModelView
    {
        public Guid Id { get; set; }
        public string QuizName { get; set; }
        public string QuizDescription { get; set; }
        public string QuizSlug { get; set; }
        public double Score { get; set; }
        public Guid QuizLanguageId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}