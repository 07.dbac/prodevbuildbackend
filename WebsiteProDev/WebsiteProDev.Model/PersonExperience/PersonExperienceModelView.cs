﻿namespace WebsiteProDev.Model.PersonExperience
{
    public class PersonExperienceModelView
    {
        public Guid PersonId { get; set; }
        public Guid ExperienceId { get; set; }
    }
}