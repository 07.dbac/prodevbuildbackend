﻿namespace WebsiteProDev.Model.PersonExperience
{
    public class PersonExperienceModel : ModelBase
    {
        public Guid? PersonId { get; set; }
        public Guid? ExperienceId { get; set; }
    }
}