﻿namespace WebsiteProDev.Model.Level
{
    public class LevelModel : ModelBase
    {
        public string LevelName { get; set; }
        public decimal Score { get; set; }
    }
}