﻿namespace WebsiteProDev.Model.Level
{
    public class LevelViewModel
    {
        public Guid Id { get; set; }
        public string LevelName { get; set; }
        public decimal Score { get; set; }
        public string UpdateByName { get; set; }
        public string CreateByName { get; set; }
    }
}