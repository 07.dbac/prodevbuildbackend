﻿namespace WebsiteProDev.Model.Role
{
    public class RoleModelView
    {
        public Guid Id { get; set; }
        public string RoleName { get; set; }
        public string NormalizeName { get; set; }
    }
}