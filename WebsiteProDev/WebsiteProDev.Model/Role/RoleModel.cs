﻿namespace WebsiteProDev.Model.Role
{
    public class RoleModel : ModelBase
    {
        public string RoleName { get; set; }
        public string NormalizeName { get; set; }
    }
}