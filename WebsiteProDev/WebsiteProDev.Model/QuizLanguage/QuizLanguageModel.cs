﻿namespace WebsiteProDev.Model.QuizLanguage
{
    public class QuizLanguageModel : ModelBase
    {
        public string QuizLanguageName { get; set; }
    }
}