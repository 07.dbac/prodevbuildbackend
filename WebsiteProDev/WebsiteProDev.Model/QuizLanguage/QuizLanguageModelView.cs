﻿namespace WebsiteProDev.Model.QuizLanguage
{
    public class QuizLanguageModelView
    {
        public Guid Id { get; set; }
        public string QuizLanguageName { get; set; }
    }
}