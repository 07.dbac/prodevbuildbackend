﻿namespace WebsiteProDev.Model.Payment
{
    public class PaymentModel : ModelBase
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime DateTime { get; set; }
        public bool Status { get; set; }
    }
}