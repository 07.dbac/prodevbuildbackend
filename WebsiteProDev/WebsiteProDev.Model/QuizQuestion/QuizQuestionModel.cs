﻿namespace WebsiteProDev.Model.QuizQuestion
{
    public class QuizQuestionModel : ModelBase
    {
        public int Status { get; set; }
        public Guid QuizId { get; set; }
        public Guid LevelId { get; set; }
    }
}