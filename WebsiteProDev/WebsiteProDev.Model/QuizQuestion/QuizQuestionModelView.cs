﻿namespace WebsiteProDev.Model.QuizQuestion
{
    public class QuizQuestionModelView
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public Guid QuizId { get; set; }
        public Guid LevelId { get; set; }
    }
}