﻿namespace WebsiteProDev.Model.Take
{
    public class TakeModelView
    {
        public Guid Id { get; set; }
        public Guid QuizId { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        public double Score { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime FinishedAt { get; set; }
    }
}