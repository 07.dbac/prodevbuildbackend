﻿namespace WebsiteProDev.Model.Experience
{
    public class ExperienceModel : ModelBase
    {
        public string Position { get; set; }
        public string Company { get; set; }
    }
}