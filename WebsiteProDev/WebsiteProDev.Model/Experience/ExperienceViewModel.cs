﻿namespace WebsiteProDev.Model.Experience
{
    public class ExperienceViewModel
    {
        public Guid Id { get; set; }
        public string Position { get; set; }
        public string Company { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}