﻿namespace WebsiteProDev.Model.Account
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}