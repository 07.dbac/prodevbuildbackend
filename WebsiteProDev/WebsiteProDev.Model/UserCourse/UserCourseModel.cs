﻿namespace WebsiteProDev.Model.UserCourse
{
    internal class UserCourseModel
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime DateBought { get; set; }
    }
}