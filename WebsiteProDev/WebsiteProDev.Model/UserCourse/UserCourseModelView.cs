﻿namespace WebsiteProDev.Model.UserCourse
{
    public class UserCourseModelView
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime DateBought { get; set; }
    }
}