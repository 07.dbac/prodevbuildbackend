﻿namespace WebsiteProDev.Model.QuizAnswer
{
    public class QuizAnswerModelView
    {
        public Guid Id { get; set; }
        public bool Correct { get; set; }
        public Guid QuizQuestionId { get; set; }
    }
}