﻿namespace WebsiteProDev.Model.QuizAnswer
{
    public class QuizAnswerModel : ModelBase
    {
        public bool Correct { get; set; }
        public Guid QuizQuestionId { get; set; }
    }
}