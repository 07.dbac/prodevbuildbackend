﻿namespace WebsiteProDev.Model.Person
{
    public class PersonModel : ModelBase
    {
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
    }
}