﻿namespace WebsiteProDev.Model.Person
{
    public class PersonViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
        public string UpdateByName { get; set; }
        public string CreateByName { get; set; }
    }
}