﻿using Microsoft.AspNetCore.Http;

namespace WebsiteProDev.DataLayer.Respone
{
    public class Response<T>
    {
        public Response()
        {
            Code = StatusCodes.Status200OK;
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public IList<T> DataList { get; set; }
        public string Error { get; set; }
    }
}