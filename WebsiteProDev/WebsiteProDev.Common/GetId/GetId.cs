﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace WebsiteProDev.Common
{
    public class GetId
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public GetId(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public Guid GetCurrentUserId()
        {
            var userIdClaim = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);

            if (userIdClaim != null && Guid.TryParse(userIdClaim.Value, out Guid userId))
            {
                return userId;
            }
            throw new InvalidOperationException("Không thể lấy thông tin user hiện tại.");
        }
    }
}
