using Microsoft.EntityFrameworkCore;
using WebsiteProDev.DataAccess.Context;
using WebsiteProDev.Repository.Course;
using WebsiteProDev.Repository.Experience;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<ProDevDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("WebsiteConnection2")));
builder.Services.AddScoped<ICourseRepository, CourseRepository>();
builder.Services.AddScoped<IExperienceRepository, ExperienceRepository>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();