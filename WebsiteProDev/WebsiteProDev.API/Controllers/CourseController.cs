﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using WebsiteProDev.DataLayer.Entity;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Course;
using WebsiteProDev.Repository.Course;

namespace WebsiteProDev.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseRepository _courseRepository;

        public CourseController(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        [HttpGet]
        public Response<CourseViewModel> GetAllCourse()
        {
            return _courseRepository.GetAllCourse();
             
        }

        [HttpGet("{id:guid}")]
        public Response<CourseViewModel> GetCourseById(Guid id)
        {
            return _courseRepository.GetCourseById(id);
        }

        [HttpPost()]
        public Response<string> AddCourse([FromForm]CourseModel course)
        {
            Common.SaveImage.SaveImageToServer(course.ImageData);
            return _courseRepository.CreateCourse(course);
        }

        [HttpDelete]
        public Response<string> DeleteCourse(Guid id)
        {
            return _courseRepository.DeleteCourse(id);
        }

            [HttpPut]
            public Response<string> UpdateCourse([FromForm]CourseModel course)
            {
            Common.SaveImage.SaveImageToServer(course.ImageData);
            return _courseRepository.UpdateCourse(course);
            }

       

    }
}
