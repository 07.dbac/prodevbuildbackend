﻿using Microsoft.AspNetCore.Mvc;
using WebsiteProDev.DataLayer.Entity;
using WebsiteProDev.DataLayer.Respone;
using WebsiteProDev.Model.Experience;
using WebsiteProDev.Repository.Experience;

namespace WebsiteProDev.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExperienceController : ControllerBase
    {
        private readonly IExperienceRepository _experienceRepository;

        public ExperienceController(IExperienceRepository experienceRepository)
        {
            _experienceRepository = experienceRepository;
        }

        [HttpGet]
        public Response<ExperienceViewModel> GetAllExperiences()
        {
            return _experienceRepository.GetAllExperiences();
        }

        [HttpGet("{id:guid}")]
        public Response<ExperienceViewModel> GetExperienceById(Guid id)
        {
            return _experienceRepository.GetExperienceById(id);
        }

        [HttpPost()]
        public Response<string> AddExperience([FromBody] ExperienceModel experience)
        {
            return _experienceRepository.CreateExperience(experience);
        }

        [HttpDelete("{id:guid}")]
        public Response<string> DeleteExperience(Guid id)
        {
            return _experienceRepository.DeleteExperienceById(id);
        }

        [HttpPut]
        public Response<string> UpdateExperience([FromBody] ExperienceModel experience)
        {
            return _experienceRepository.UpdateExperience(experience);
        }
    }
}
