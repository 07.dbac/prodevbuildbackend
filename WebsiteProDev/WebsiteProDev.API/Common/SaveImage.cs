﻿namespace WebsiteProDev.API.Common
{
    public class SaveImage
    {
        public static void SaveImageToServer(IFormFile formFile)
        {
            if (formFile != null)
            {
                //Get url To Save
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "Image", formFile.FileName);

                using (var stream = new FileStream(SavePath, FileMode.Create))
                {
                    formFile.CopyTo(stream);
                }
            }
        }
    }
}
